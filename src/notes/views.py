from django.shortcuts import render, get_object_or_404, redirect
from .models import Note
from .forms import NoteModelForm


def list_view(request):
    form = NoteModelForm()
    if request.method == "POST":
        form = NoteModelForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('home')
    todo_list = Note.objects.filter(finished=False)
    finished_list = Note.objects.filter(finished=True)

    template = 'notes/list.html'
    context = {
        'todo': todo_list,
        'finished': finished_list,
        'form': form,
    }

    return render(request, template, context)


def finished_item(request, id):
    item = get_object_or_404(Note, id=id)
    item.finished = True
    item.save()

    return redirect('home')


def recover_item(request, id):
    item = get_object_or_404(Note, id=id)
    item.finished = False
    item.save()

    return redirect('home')


def delete_item(request, id):
    item = get_object_or_404(Note, id=id)
    item.delete()

    return redirect('home')
