from django.db import models
from django.shortcuts import reverse

LABEL_CHOICE = (
    ('P', 'primary'),
    ('SE', 'secondary'),
    ('S', 'success'),
    ('DR', 'danger'),
    ('W', 'warning'),
    ('I', 'info'),
    ('L', 'light'),
    ('D', 'dark'),
)


class Note(models.Model):
    title = models.CharField(max_length=100)
    due_date = models.DateField()
    label = models.CharField(choices=LABEL_CHOICE, max_length=2)
    finished = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def get_finished_item_url(self):
        return reverse("finished_item", kwargs={
            'id': self.id
        })

    def get_recover_item_url(self):
        return reverse("recover_item", kwargs={
            'id': self.id
        })

    def get_delete_item_url(self):
        return reverse("delete_item", kwargs={
            'id': self.id
        })
