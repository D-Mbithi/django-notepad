from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from notes.views import list_view, finished_item, delete_item, recover_item

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', list_view, name='home'),
    path('finished_item/<id>', finished_item, name='finished_item'),
    path('recover_item/<id>', recover_item, name='recover_item'),
    path('delete_item/<id>', delete_item, name='delete_item'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
